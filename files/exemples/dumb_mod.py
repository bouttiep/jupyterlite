import scipy
import numpy as np
from matplotlib import pyplot

def create_array(size =10):
    return np.arange(size)

def plot(array):
    pyplot.plot(array)
    pyplot.show()

if __name__ == '__main__':
    create_array(100)
    plot(100)